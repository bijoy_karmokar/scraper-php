<?php
require 'init.php';

$mode = isset($_GET['mode'])? $_GET['mode']: null;

if (isset($mode)) {
	if ($mode == 'update') {
		
		$rawPost = $_POST;
		
		if (isset($_POST['importance']) && empty($_POST['importance'][0])) {
			unset($_POST['importance']);
		}
		if (isset($_POST['mode'])) {
			unset($_POST['mode']);
		}
		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
		}
		//$scraper = new Scraper;
		$post_data = [
			'method' => $rawPost['mode'],
			'post_data' => $scraper->postToString($_POST),
			'post_obj' => json_encode($_POST)
		];

		$result = $db->query("SELECT * From schedule where method = '".$rawPost['mode']."'", 1);

		if ($result['result'] == null) {
			$post_data['last_exec'] = null;
			$post_data['created_at'] = date('Y-m-d H:i:s');
			$post_data['updated_at'] = null;
			
			if ($db->insert('schedule', $post_data)) {
				header('Location: schedule-scraper.php?mode='.$rawPost['mode']);
			} else {
				echo 'not inserted';
			}
		} else {
			$post_data['last_exec'] = $result['result']->last_exec;
			$post_data['created_at'] = $result['result']->created_at;
			$post_data['updated_at'] = date('Y-m-d H:i:s');
			if ($db->update('schedule', $post_data, 'id', $result['result']->id)) {
				echo "string";
				header('Location: schedule-scraper.php?mode='.$rawPost['mode']);
			} else {
				echo 'not updated';
			}
		}

		if (isset($result['error'])) {
			header('Location: schedule-scraper.php?mode='.$rawPost['mode']);
		}
	} elseif ($mode == 'weekly') {
		$data = $db->query("SELECT post_obj From schedule where method = 'weekly'", 1);
	} elseif ($mode == 'daily') {
		$data = $db->query("SELECT post_obj From schedule where method = 'daily'", 1);
	} else {
		echo 'method not selected';
		exit;
	}

	if ($data['result'] !== null) {
		$result = json_decode($data['result']->post_obj);
	} else {
		$result = [];
	}
	$post = new stdClass;
	$post->dateFrom = isset($result->dateFrom)? $result->dateFrom: '';
	$post->dateTo = isset($result->dateTo)? $result->dateTo: '';
	$post->importance = isset($result->importance)? $result->importance: [];
	$post->timeFilter = isset($result->timeFilter)? $result->timeFilter: '';
	$post->timeZone = isset($result->timeZone)? $result->timeZone: '39';
	$post->country = isset($result->country)? $result->country: [];
	$post->category = isset($result->category)? $result->category: [];
	//var_dump($post);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Php data Scraper</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="./index.php">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Cron job
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="./schedule-scraper.php?mode=daily">Daily</a>
						<a class="dropdown-item" href="./schedule-scraper.php?mode=weekly">Weekly</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link disabled" href="#">Disabled</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container py-3">
		<div class="row">
			<div class="col-12 col-sm 12 col-md-10 col-lg-8 m-auto">
				<div class="card">
					<div class="card-header">
						<div class="card-title">Setup <?php echo $_GET['mode']; ?> Settings</div>
					</div>
					<div class="card-body">
						<form method="post" action="schedule-scraper.php?mode=update">
							<input type="hidden" name="mode" value="<?php echo $mode; ?>">
							<input type="hidden" name="currentTab" value="custom">
							<input type="hidden" name="limit_from" value="0">
							<div class="row">
								<div class="col-12 col-sm-6">
									<label for="datepicker">Select Time:</label>
									<div class="input-daterange input-group" id="datepicker">
										<input type="text" class="input-sm form-control" name="dateFrom" value="<?php echo isset($post->dateFrom)?$post->dateFrom:'';?>">
										<span class="input-group-addon">to</span>
										<input type="text" class="input-sm form-control" name="dateTo" value="<?php echo isset($post->dateTo)?$post->dateTo:'';?>">
									</div>
								</div>
								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label for="timezone">Time-zone</label>
										<select class="form-control" name="timeZone" id="timezone">
											<option value="2" <?php echo (@$post->timeZone == '2')?'selected':'';?>>(GMT -11:00) Midway Island</option>
											<option value="35" <?php echo (@$post->timeZone == '35')?'selected':'';?>>(GMT -11:00) Samoa</option>
											<option value="3" <?php echo (@$post->timeZone == '3')?'selected':'';?>>(GMT -10:00) Hawaii</option>
											<option value="4" <?php echo (@$post->timeZone == '4')?'selected':'';?>>(GMT -8:00) Alaska</option>
											<option value="37" <?php echo (@$post->timeZone == '37')?'selected':'';?>>(GMT -7:00) Arizona</option>
											<option value="36" <?php echo (@$post->timeZone == '36')?'selected':'';?>>(GMT -7:00) Baja California</option>
											<option value="5" <?php echo (@$post->timeZone == '5')?'selected':'';?>>(GMT -7:00) Pacific Time (US &amp; Canada)</option>
											<option value="38" <?php echo (@$post->timeZone == '38')?'selected':'';?>>(GMT -6:00) Chihuahua, La Paz, Mazatlan</option>
											<option value="6" <?php echo (@$post->timeZone == '6')?'selected':'';?>>(GMT -6:00) Mountain Time (US &amp; Canada)</option>
											<option value="42" <?php echo (@$post->timeZone == '42')?'selected':'';?>>(GMT -5:00) Bogota, Lima, Quito</option>
											<option value="39" <?php echo (@$post->timeZone == '39')?'selected':'';?>>(GMT -5:00) Central America</option>
											<option value="7" <?php echo (@$post->timeZone == '7')?'selected':'';?>>(GMT -5:00) Central Time (US &amp; Canada)</option>
											<option value="40" <?php echo (@$post->timeZone == '40')?'selected':'';?>>(GMT -5:00) Guadalajara, Mexico City, Monterrey</option>
											<option value="41" <?php echo (@$post->timeZone == '41')?'selected':'';?>>(GMT -5:00) Saskatchewan</option>
											<option value="44" <?php echo (@$post->timeZone == '44')?'selected':'';?>>(GMT -4:00) Asuncion</option>
											<option value="9" <?php echo (@$post->timeZone == '9')?'selected':'';?>>(GMT -4:00) Caracas</option>
											<option value="45" <?php echo (@$post->timeZone == '45')?'selected':'';?>>(GMT -4:00) Cuiaba</option>
											<option value="8" <?php echo (@$post->timeZone == '8')?'selected':'';?>>(GMT -4:00) Eastern Time (US &amp; Canada)</option>
											<option value="46" <?php echo (@$post->timeZone == '46')?'selected':'';?>>(GMT -4:00) Georgetown, La Paz, Manaus, San Juan</option>
											<option value="43" <?php echo (@$post->timeZone == '43')?'selected':'';?>>(GMT -4:00) Indiana (East)</option>
											<option value="47" <?php echo (@$post->timeZone == '47')?'selected':'';?>>(GMT -4:00) Santiago</option>
											<option value="10" <?php echo (@$post->timeZone == '10')?'selected':'';?>>(GMT -3:00) Atlantic Time (Canada)</option>
											<option value="12" <?php echo (@$post->timeZone == '12')?'selected':'';?>>(GMT -3:00) Brasilia</option>
											<option value="48" <?php echo (@$post->timeZone == '48')?'selected':'';?>>(GMT -3:00) Buenos Aires</option>
											<option value="49" <?php echo (@$post->timeZone == '49')?'selected':'';?>>(GMT -3:00) Cayenne, Fortaleza</option>
											<option value="51" <?php echo (@$post->timeZone == '51')?'selected':'';?>>(GMT -3:00) Montevvalueeo</option>
											<option value="11" <?php echo (@$post->timeZone == '11')?'selected':'';?>>(GMT -2:30) Newfoundland</option>
											<option value="50" <?php echo (@$post->timeZone == '50')?'selected':'';?>>(GMT -2:00) Greenland</option>
											<option value="53" <?php echo (@$post->timeZone == '53')?'selected':'';?>>(GMT -1:00) Cape Verde Is.</option>
											<option value="14" <?php echo (@$post->timeZone == '14')?'selected':'';?>>(GMT) Azores</option>
											<option value="55" <?php echo (@$post->timeZone == '55')?'selected':'';?>>(GMT) Coordinated Universal Time</option>
											<option value="56" <?php echo (@$post->timeZone == '56')?'selected':'';?>>(GMT) Monrovia, Reykjavik</option>
											<option value="54" <?php echo (@$post->timeZone == '54')?'selected':'';?>>(GMT +1:00) Casablanca</option>
											<option value="15" <?php echo (@$post->timeZone == '15')?'selected':'';?>>(GMT +1:00) Dublin, Edinburgh, Lisbon, London</option>
											<option value="166" <?php echo (@$post->timeZone == '166')?'selected':'';?>>(GMT +1:00) Lagos</option>
											<option value="60" <?php echo (@$post->timeZone == '60')?'selected':'';?>>(GMT +1:00) West Central Africa</option>
											<option value="16" <?php echo (@$post->timeZone == '16')?'selected':'';?>>(GMT +2:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
											<option value="57" <?php echo (@$post->timeZone == '57')?'selected':'';?>>(GMT +2:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
											<option value="58" <?php echo (@$post->timeZone == '58')?'selected':'';?>>(GMT +2:00) Brussels, Copenhagen, Madrvalue, Paris</option>
											<option value="65" <?php echo (@$post->timeZone == '65')?'selected':'';?>>(GMT +2:00) Cairo</option>
											<option value="67" <?php echo (@$post->timeZone == '67')?'selected':'';?>>(GMT +02:00) Johannesburg</option>
											<option value="59" <?php echo (@$post->timeZone == '59')?'selected':'';?>>(GMT +2:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
											<option value="61" <?php echo (@$post->timeZone == '61')?'selected':'';?>>(GMT +2:00) Windhoek</option>
											<option value="62" <?php echo (@$post->timeZone == '62')?'selected':'';?>>(GMT +3:00) Amman</option>
											<option value="71" <?php echo (@$post->timeZone == '71')?'selected':'';?>>(GMT +3:00) Baghdad</option>
											<option value="64" <?php echo (@$post->timeZone == '64')?'selected':'';?>>(GMT +3:00) Beirut</option>
											<option value="66" <?php echo (@$post->timeZone == '66')?'selected':'';?>>(GMT +3:00) Damascus</option>
											<option value="68" <?php echo (@$post->timeZone == '68')?'selected':'';?>>(GMT +3:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
											<option value="63" <?php echo (@$post->timeZone == '63')?'selected':'';?>>(GMT +3:00) Istanbul</option>
											<option value="17" <?php echo (@$post->timeZone == '17')?'selected':'';?>>(GMT +3:00) Jerusalem</option>
											<option value="70" <?php echo (@$post->timeZone == '70')?'selected':'';?>>(GMT +3:00) Kuwait, Riyadh</option>
											<option value="18" <?php echo (@$post->timeZone == '18')?'selected':'';?>>(GMT +3:00) Moscow, St. Petersburg, Volgograd</option>
											<option value="72" <?php echo (@$post->timeZone == '72')?'selected':'';?>>(GMT +3:00) Nairobi</option>
											<option value="20" <?php echo (@$post->timeZone == '20')?'selected':'';?>>(GMT +4:00) Abu Dhabi, Dubai, Muscat</option>
											<option value="73" <?php echo (@$post->timeZone == '73')?'selected':'';?>>(GMT +4:00) Baku</option>
											<option value="21" <?php echo (@$post->timeZone == '21')?'selected':'';?>>(GMT +4:30) Kabul</option>
											<option value="19" <?php echo (@$post->timeZone == '19')?'selected':'';?>>(GMT +4:30) Tehran</option>
											<option value="22" <?php echo (@$post->timeZone == '22')?'selected':'';?>>(GMT +5:00) Ekaterinburg</option>
											<option value="77" <?php echo (@$post->timeZone == '77')?'selected':'';?>>(GMT +5:00) Karachi</option>
											<option value="23" <?php echo (@$post->timeZone == '23')?'selected':'';?>>(GMT +5:30) Chennai, Kolkata, Mumbai, New Delhi</option>
											<option value="79" <?php echo (@$post->timeZone == '79')?'selected':'';?>>(GMT +5:30) Colombo</option>
											<option value="24" <?php echo (@$post->timeZone == '24')?'selected':'';?>>(GMT +5:45) Kathmandu</option>
											<option value="25" <?php echo (@$post->timeZone == '25')?'selected':'';?>>(GMT +6:00) Dhaka</option>
											<option value="26" <?php echo (@$post->timeZone == '26')?'selected':'';?>>(GMT +6:30) Yangon (Rangoon)</option>
											<option value="27" <?php echo (@$post->timeZone == '27')?'selected':'';?>>(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
											<option value="28" <?php echo (@$post->timeZone == '28')?'selected':'';?>>(GMT +8:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
											<option value="178" <?php echo (@$post->timeZone == '178')?'selected':'';?>>(GMT +08:00) Manila</option>
											<option value="113" <?php echo (@$post->timeZone == '113')?'selected':'';?>>(GMT +8:00) Singapore</option>
											<option value="29" <?php echo (@$post->timeZone == '29')?'selected':'';?>>(GMT +9:00) Osaka, Sapporo, Tokyo</option>
											<option value="88" <?php echo (@$post->timeZone == '88')?'selected':'';?>>(GMT +9:00) Seoul</option>
											<option value="30" <?php echo (@$post->timeZone == '30')?'selected':'';?>>(GMT +9:30) Adelavaluee</option>
											<option value="91" <?php echo (@$post->timeZone == '91')?'selected':'';?>>(GMT +10:00) Brisbane</option>
											<option value="31" <?php echo (@$post->timeZone == '31')?'selected':'';?>>(GMT +10:00) Canberra, Melbourne, Sydney</option>
											<option value="94" <?php echo (@$post->timeZone == '94')?'selected':'';?>>(GMT +10:00) Vladivostok</option>
											<option value="32" <?php echo (@$post->timeZone == '32')?'selected':'';?>>(GMT +11:00) Solomon Is., New Caledonia</option>
											<option value="33" <?php echo (@$post->timeZone == '33')?'selected':'';?>>(GMT +12:00) Auckland, Weloptionngton</option>
											<option value="1" <?php echo (@$post->timeZone == '1')?'selected':'';?>>(GMT +12:00) Eniwetok, Kwajalein</option>
										</select>
									</div>
								</div>
							</div>
							<div class="collapse" id="collapseExample">
								<div class="row">
									<div class="col-12 col-sm-6">
										<div id="calendarFilterBox_time" class="ecoFilterBox">
											<div class="left float_lang_base_1">
												<p class="arial_14 bold">Time:</p>
											</div>
											<div class="right float_lang_base_2">
												<fieldset><input type="radio" id="timeFiltertimeRemain" name="timeFilter" value="timeRemain" checked="checked"<?php echo ('timeRemain' == $post->timeFilter)?'checked':''?>><label for="timeFiltertimeRemain">Display time remaining until announcement</label></fieldset>
												<fieldset><input type="radio" id="timetimeOnly" name="timeFilter" value="timeOnly"<?php echo ('timeOnly' == $post->timeFilter)?'checked':''?>><label for="timeFiltertimeOnly">Display time only</label></fieldset>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="col-12 col-sm-6 mr-auto">
										<div class="form-group">
											<label for="important">Important</label>
											<select class="form-control" id="importance[]" name="importance[]">
												<option value="">Select Important</option>
												<option value="1" <?php echo (array_search('1', $post->importance) !== false)?'selected':'';?>>1 star</option>
												<option value="2" <?php echo (array_search('2', $post->importance) !== false)?'selected':'';?>>2</option>
												<option value="3" <?php echo (array_search('3', $post->importance) !== false)?'selected':'';?>>3</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-6">
										<div class="row justify-content-between mx-0 my-3">
											<button class="btn btn-primary select_all_country">Check all</button>
											<button class="btn btn-primary unselect_all_country">Uncheck all</button>
										</div>
										<div class="form-group">
											<ul class="list-group country">
												<li class="list-group-item"><input id="country5" name="country[]" type="checkbox" value="5" <?php echo (array_search('5', $post->country)!== false)?'checked':'';?>><span class="ceFlags USA">&nbsp;</span><label for="country5">United States</label></li>
												<li class="list-group-item"><input id="country6" name="country[]" type="checkbox" value="6" <?php echo (array_search('6', $post->country) !== false)?'checked':'';?>><span class="ceFlags Canada">&nbsp;</span><label for="country6">Canada</label></li>
												<li class="list-group-item"><input id="country4" name="country[]" type="checkbox" value="4" <?php echo (array_search('4', $post->country) !== false)?'checked':'';?>><span class="ceFlags UK">&nbsp;</span><label for="country4">United Kingdom</label></li>
												<li class="list-group-item"><input id="country72" name="country[]" type="checkbox" value="72" <?php echo (array_search('72', $post->country) !== false)?'checked':'';?>><span class="ceFlags Europe">&nbsp;</span><label for="country72">Euro Zone</label></li>
												<li class="list-group-item"><input id="country25" name="country[]" type="checkbox" value="25" <?php echo (array_search('25', $post->country) !== false)?'checked':'';?>><span class="ceFlags Australia">&nbsp;</span><label for="country25">Australia</label></li>
												<li class="list-group-item"><input id="country43" name="country[]" type="checkbox" value="43" <?php echo (array_search('43', $post->country) !== false)?'checked':'';?>><span class="ceFlags New_Zealand">&nbsp;</span><label for="country43">New Zealand</label></li>
												<li class="list-group-item"><input id="country35" name="country[]" type="checkbox" value="35" <?php echo (array_search('35', $post->country) !== false)?'checked':'';?>><span class="ceFlags Japan">&nbsp;</span><label for="country35">Japan</label></li>
												<li class="list-group-item"><input id="country12" name="country[]" type="checkbox" value="12" <?php echo (array_search('12', $post->country) !== false)?'checked':'';?>><span class="ceFlags Switzerland">&nbsp;</span><label for="country12">Switzerland</label></li>
											</ul>
										</div>
									</div>
									<div class="col-12 col-sm-6">
										<div id="calendarFilterBox_category" class="ecoFilterBox">
											<div class="row justify-content-between mx-0 my-3">
												<button class="btn btn-primary select_all_category">Check all</button>
												<button class="btn btn-primary unselect_all_category">Uncheck all</button>
											</div>
											<div class="right float_lang_base_2 ">
												<ul class="list-group category">
													<li class="list-group-item"><input id="category_employment" name="category[]" type="checkbox" value="_employment" <?php echo (array_search('_employment', $post->category) !== false)?'checked':'';?>><label for="category_employment">Employment</label></li>
													<li class="list-group-item"><input id="category_economicActivity" name="category[]" type="checkbox" value="_economicActivity" <?php echo (array_search('_economicActivity', $post->category) !== false)?'checked':'';?>><label for="category_economicActivity">Economic Activity</label></li>
													<li class="list-group-item"><input id="category_inflation" name="category[]" type="checkbox" value="_inflation" <?php echo (array_search('_inflation', $post->category) !== false)?'checked':'';?>><label for="category_inflation">Inflation</label></li>
													<li class="list-group-item"><input id="category_credit" name="category[]" type="checkbox" value="_credit" <?php echo (array_search('_credit', $post->category) !== false)?'checked':'';?>><label for="category_credit">Credit</label></li>
													<li class="list-group-item"><input id="category_centralBanks" name="category[]" type="checkbox" value="_centralBanks" <?php echo (array_search('_centralBanks', $post->category) !== false)?'checked':'';?>><label for="category_centralBanks">Central Banks</label></li>
													<li class="list-group-item"><input id="category_confidenceIndex" name="category[]" type="checkbox" value="_confidenceIndex" <?php echo (array_search('_confidenceIndex', $post->category) !== false)?'checked':'';?>><label for="category_confidenceIndex">Confidence Index</label></li>
													<li class="list-group-item"><input id="category_balance" name="category[]" type="checkbox" value="_balance" <?php echo (array_search('_balance', $post->category) !== false)?'checked':'';?>><label for="category_balance">Balance</label></li>
													<li class="list-group-item"><input id="category_Bonds" name="category[]" type="checkbox" value="_Bonds" <?php echo (array_search('_Bonds', $post->category) !== false)?'checked':'';?>><label for="category_Bonds">Bonds</label></li>
												</ul>
											</div>
											<div class="clear"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row justify-content-between mx-0">
								<div class="form-group">
									<input type="submit" name="submit" class="btn btn-primary"></input>
								</div>
								<div class="text-right">
									<button id="filter" class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">filter</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="./assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="./assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="./assets/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#datepicker').datepicker({
				format: "yyyy-mm-dd"
			});
			
			$('.select_all_country').click(function(event){
				event.preventDefault();
				$('.country > li> input[type=checkbox]').prop('checked', true);
			});

			$('.unselect_all_country').click(function(event){
				event.preventDefault();
				$('.country > li> input[type=checkbox]').prop('checked', false);
			});

			$('.select_all_category').click(function(event){
				event.preventDefault();
				$('.category > li> input[type=checkbox]').prop('checked', true);
			});

			$('.unselect_all_category').click(function(event){
				event.preventDefault();
				$('.category > li> input[type=checkbox]').prop('checked', false);
			});
		})
	</script>
</body>
</html>