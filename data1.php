<?php
require 'init.php';
if (isset($_GET))	 {
	if (isset($_GET['currency'])) {
		$currency = strtoupper($_GET['currency']);
		$db->where('currency', $currency);
	}

	if (isset($_GET['country'])) {
		$country = strtoupper($_GET['country']);
		if (strlen($country) < 3) {
			$db->where('currency', $country);
		}
	}

	if (isset($_GET['imp'])) {
		$imp = $_GET['imp'];
		$db->where('imp', $imp);
	}

	if (isset($_GET['from_date']) && isset($_GET['end_date'])) {
		$from_date = date('Y-m-d', strtotime($_GET['from_date']));
		$end_date = date('Y-m-d', strtotime($_GET['end_date']));
		
		$db->where('date', [$from_date, $end_date], 'BETWEEN');
	} elseif (isset($_GET['from_date'])) {
		$from_date = date('Y-m-d', strtotime($_GET['from_date']));
		
		$db->where('date', $from_date, '>=');
	} elseif (isset($_GET['end_date'])) {
		$end_date = date('Y-m-d', strtotime($_GET['end_date']));
		
		$db->where('date', $end_date, '<=');
	}

	if (isset($_GET['from_time']) && isset($_GET['end_time'])) {
		$from_time = date('H:i:s', strtotime($_GET['from_time']));
		$end_time = date('H:i:s', strtotime($_GET['end_time']));
		
		$db->where('time', [$from_time, $end_time], 'BETWEEN');
	} elseif (isset($_GET['from_time'])) {
		$from_time = date('H:i:s', strtotime($_GET['from_time']));
		
		$db->where('time', $from_time, '>=');
	} elseif (isset($_GET['end_time'])) {
		$end_time = date('H:i:s', strtotime($_GET['end_time']));
		
		$db->where('time', $end_time, '<=');
	}

	try {
		$data = $db->get('data');
		if ($db->getLastErrno() === 0) {
			$response = [
				'error' => false,
				'total' => count($data),
				'data' => $data,
			];
		} else {
			$response = [
				'error' => true,
				'error_message' => $db->getLastError(),
			];
		}
		echo json_encode($response);
		exit;
	} catch (Exception $e) {
		$response = [
			'error' => true,
			'error_message' => $e,
		];
		echo json_encode($response);
		exit;
	}
} else {
	echo json_encode([
		'error' => true,
		'error_message' => 'no parameter selected'
	]);
}
?>