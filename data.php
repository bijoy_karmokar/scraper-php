<?php
require 'init.php';

if (isset($_GET))	 {
	$post = [
		'currentTab' => 'custom',
		'limit_from' => '0',
		'timeFilter' => 'timeRemain',
	];
	
	if (isset($_GET['currency'])) {
		if ($_GET['currency'] == 'chf') {
			$post['country'][] = ['12'];
		} elseif ($_GET['currency'] == 'eur') {
			$post['country'][] = '72';
		} elseif ($_GET['currency'] == 'aud') {
			$post['country'][] = '25';
		} elseif ($_GET['currency'] == 'cad') {
			$post['country'][] = '6';
		} elseif ($_GET['currency'] == 'nzd') {
			$post['country'][] = '43';
		} elseif ($_GET['currency'] == 'jpy') {
			$post['country'][] = '35';
		} else {
			$post['country'][] = '5';
		}
	}

	if (isset($_GET['imp'])) {
		$post['importance'][] = $_GET['imp'];
	}
	
	if (isset($_GET['from_date'])) {
		$post['dateFrom'] = $_GET['from_date'];
	} 

	if (isset($_GET['end_date'])) {
		$post['dateTo'] = $_GET['end_date'];
	}

	$post['timeZone'] = (isset($_GET['timezone']))? $_GET['timezone'] : '39';
	$data = $scraper->get_curl($post);

	$response = [
		'error' => false,
		'data' => [],
	];
	if (isset($_GET['from_time']) || isset($_GET['end_time'])) {
		foreach ($data->fetch_result as $key => $value) {
			if (isset($_GET['from_time']) && isset($_GET['end_time'])) {
				$timeStart = date('H:i', strtotime($_GET['from_time']));
				$endTime = date('H:i', strtotime($_GET['end_time']));
				if (($value->time > $timeStart) && ($value->time < $endTime)) {
					array_push($response['data'], $value);
				}	
			} elseif (isset($_GET['from_time'])) {
				if (date('H:i', strtotime($_GET['from_time'])) < date('H:i', strtotime($value->time))) {
					array_push($response['data'], $value);
				}
			} elseif (isset($_GET['end_time'])) {
				if (date('H:i', strtotime($_GET['end_time'])) > date('H:i', strtotime($value->time))) {
					array_push($response['data'], $value);
				}
			}
		}
	} else {
		$response['data'] = $data->fetch_result;
	}
	$response['total'] = count($response['data']);
	echo json_encode($response);
	exit;
} else {
	echo json_encode([
		'error' => true,
		'error_message' => 'no parameter selected'
	]);
}
?>