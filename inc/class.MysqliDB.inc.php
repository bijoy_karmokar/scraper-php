<?php
class DB {
	private $host = "localhost";
	private $username = "root";
	private $password = "";
	private $database = "scraper";
	private $DbCon;

	public function __construct() 
	{
		$con = new mysqli($this->host,$this->username,$this->password,$this->database);
		
		if ($con) {
			$this->DbCon = $con;
			return true;
		}else{
			return false;
		}
	}
	
	public function query($query, $limit = 0)
	{
		$data = [
			'error' => false,
			'result' => []
		];
		try {
			$result = $this->DbCon->query($query);
			if ($result) {
				if (isset($result->num_rows)) {
					if ($result->num_rows > 0) {
						if ($limit == 0) {
							while ($row = $result->fetch_assoc()) {
								array_push($data['result'], $row);
							}
						} else {
							$data['result'] = $result->fetch_object();
						}
					} else {
						$data['result'] = null;
					}
				} elseif ($result === true) {
					$data['result'] = true;
				} else {
					$data['result'] = false;
				}
			} else {
				$data = [
					'error' => true,
					'result' => $this->DbCon->error
				];
			}
			return $data;
		} catch (Exception $e) {
			return $e;
		}
	}

	public function insert($table, $data){
		$column_data = '';
		foreach ($data as $key => $value) {
			$column[] = $key;
			$column_data .= "'".$value."', ";
		}
		$column_data = rtrim(trim($column_data), ',');
		$sql = "INSERT INTO ".$table." (".implode(',', $column).") VALUES (".$column_data.")";
		return $this->query($sql);
	}

	public function update($table, $data, $where_column_name, $where_column_value){
		$column_data = '';
		foreach ($data as $key => $value) {
			$column_data .= $key." = '".$value."',";
		}

		$column_data = rtrim(trim($column_data), ',');
		$sql = "UPDATE ".$table." SET ".$column_data." WHERE ".$where_column_name." = '$where_column_value'";
		return $this->query($sql);
	}
}
// $a= new Database();
// $a->connect();
// $upd=array('username'=>'Badshah',
// 	'password'=>'badshah',
// 	'email'=>'badshah@gmail.com');
// $a->update('user',$upd,array('id=3','id=4','id=5','id=6'));
//$a->delete('user',' id = 1');
//$ins=array('','Badshah','badshah','badshah@gmail.com');
//$a->insert('user',$ins,null);
//$ab=$a->select('user');
//while($a=$ab->fetch_array()){
//	echo $a[0]."<br />";
//}

?>