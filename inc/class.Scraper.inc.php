<?php

/**
 * 
 */

class Scraper
{
	public function __construct($db) {
		$this->db = $db;
	}

	public function get_curl ($post, $post_data_update = false, $dbUpdate = false, $history = false)
	{
		$original_post = $post;

		if ($post_data_update === true) {
			$post = json_decode($post);
			if (isset($post->importance) && empty($post->importance[0])) {
				unset($post->importance);
			}

			$post_string = Scraper::postToString($post);
		} else {
			if (isset($post['importance']) && empty($post['importance'][0])) {
				unset($post['importance']);
			}
			$post_string = Scraper::postToString($post);
		}
		
		$url ='https://www.investing.com/economic-calendar/Service/getCalendarFilteredData';
		$proxy = '51.68.207.81:80';
		$useragent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36';
		$headers = array(
			'referer: https://www.investing.com/economic-calendar/',
			'user-agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36',
			'x-requested-with: XMLHttpRequest',
			'content-type: application/x-www-form-urlencoded',
			'content-length: '.strlen($post_string)
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		//curl_setopt($curl, CURLOPT_PROXY, $proxy);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);           
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);   
		if($useragent)curl_setopt($curl, CURLOPT_USERAGENT,$useragent);             
		if($headers)curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_string);

		$response = curl_exec($curl);       

		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$response = substr($response, $header_size);

		curl_close($curl);
		
		$obj = Scraper::responseToObject($response, true);
		
		if ($dbUpdate === true) {
			$response = Scraper::insertOrUpdate($obj['object']);
		}

		if ($obj['isMore'] === true) {
			$post["limit_from"] = $post["limit_from"] + 1;
			Scraper::get_curl($post, false, true);
		}

		$file_data = file_get_contents(BASEPATH.'/'.HISTORY_FILE);
		$file_data = json_decode($file_data);

		$data = [
			'post_data' => $post_string,
			'post_obj' => $original_post,
			'total_obj' => count($file_data->fetch_result),
			'fetch_result' => $file_data->fetch_result,
		];

		if ($history === true) {
			file_put_contents(BASEPATH.'/'.HISTORY_FILE, json_encode($data));
		}
		
		return $data;
	}

	public function responseToObject($resp , $is_obj_save_in_txt = false)
	{
		$resp = json_decode($resp);
		
		$pids = $resp->pids;
		$html = str_replace('\u00a0 ', '', $resp->data);
		$html = str_replace('  ', '', $html);
		
		$isMore = $resp->bind_scroll_handler;
		
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		@$dom->loadHTML($html);
		
		$object = [];
		
		foreach ($pids as $key => $value) {
			$eventValue = explode('-', $value);
			$rowid = 'eventRowId_'.$eventValue[1];
			$rowid = str_replace(':', '', $rowid);
			
			$row = $dom->getElementById($rowid);
			$cells = $row->getElementsByTagName('td');
			
			$currency = htmlentities(trim($cells->item(1)->nodeValue), null, 'utf-8');
			$currency = str_replace("&nbsp; ", '', $currency);
			
			$actual = htmlentities(trim($cells->item(4)->nodeValue), null, 'utf-8');
			$actual = str_replace("&nbsp;", '', $actual);
			
			$forecast = htmlentities(trim($cells->item(5)->nodeValue), null, 'utf-8');
			$forecast = str_replace("&nbsp;", '', $forecast);
			
			$previous = htmlentities(trim($cells->item(6)->nodeValue), null, 'utf-8');
			$previous = str_replace("&nbsp;", '', $previous);
			
			$imp = $cells->item(2)->getAttribute('data-img_key');
			$imp = explode('bull', $imp);
			
			$date = $row->getAttribute('data-event-datetime');
			$date = date('Y-m-d', strtotime($date));
			
			$obj = new stdClass;
			$obj->rid = $rowid;
			$obj->date = $date;
			$obj->time = trim($cells->item(0)->nodeValue);
			$obj->currency = $currency;
			$obj->imp = $imp[1];
			$obj->event = trim($cells->item(3)->nodeValue);
			$obj->actual = $actual;
			$obj->forecast = $forecast;
			$obj->previous = $previous;

			array_push($object, $obj);
			if ($is_obj_save_in_txt === true) {
				$file_data = file_get_contents(BASEPATH.'/'.HISTORY_FILE);
				
				if ($file_data == null) {
					$data = [
						'fetch_result' => []
					];
					
					file_put_contents(BASEPATH.'/'.HISTORY_FILE, json_encode($data));
					
					$file_data = file_get_contents(BASEPATH.'/'.HISTORY_FILE);
					$file_data = json_decode($file_data);	
				} else {
					$file_data = json_decode($file_data);
				}

				array_push($file_data->fetch_result, $obj);
				
				file_put_contents(BASEPATH.'/'.HISTORY_FILE, json_encode($file_data));
			}
		}

		return [
			'object' => $object,
			'isMore' => $isMore,
		];
	}
	
	public function insertOrUpdate($object)
	{
		$response = [];
		foreach ($object as $key => $value) {
			$result = $this->db->query("SELECT * FROM data WHERE rid = '".$value->rid."'", 1);

			if ($result['result'] == null) {
				$data = [
					'rid' => $value->rid,
					'date' => date('Y-m-d', strtotime($value->date)),
					'time' => $value->time,
					'currency' =>$value->currency,
					'imp' =>$value->imp,
					'event' =>$value->event,
					'actual' =>$value->actual,
					'forecast' =>$value->forecast,
					'previous' =>$value->previous,
					'object' => json_encode($value)
				];
				try {
					$result = $this->db->insert('data', $data);
					if ($result['error']) {
						$response['error'] = $result['result'];
					}
				} catch (Exception $e) {
					$response['error'] = $e;
				}
			} else {
				if (json_decode($result['result']->object) == json_encode($value)) {
					$key++;
				} else {
					Scraper::dataUpdate($value);
				}
			}
		}
	}

	public function dataUpdate($object)
	{
		$response = [];
		$data = [
			'date' => date('Y-m-d', strtotime($object->date)),
			'time' => $object->time,
			'currency' => $object->currency,
			'imp' => $object->imp,
			'event' => $object->event,
			'actual' => $object->actual,
			'forecast' => $object->forecast,
			'previous' => $object->previous,
			'object' => json_encode($object),
			'updated_at' => date('Y-m-d H:i:s')
		];
		try {
			$row = $this->db->update('data', $data, 'rid', $object->rid);
			$response = $row['result'];
		} catch (Exception $e) {
			$response['error'] = $e;
		}
		return $response;
	}

	public function postToString($post_parm = [])
	{
		$post = '';
		
		foreach ($post_parm as $key => $value) {
			if (is_array($value)) {
				foreach ($value as $array_value) {
					$post .= $key.'[]='.$array_value.'&';
				}
			} else {
				$post .= $key.'='.$value.'&';
			}
		}

		return $post;
	}
}
?>