<?php

//error_reporting(0);
session_start();

ini_set('max_execution_time', 0);
ini_set('memory_limit', '512M');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require './inc/class.Scraper.inc.php';
require './inc/class.MysqliDB.inc.php';

if (!file_exists('168e03db9930da445b22ddb9e2ceab7f.txt')) {
	$file = fopen('168e03db9930da445b22ddb9e2ceab7f.txt', 'w');
	fclose($file);
}



define('HISTORY_FILE', '168e03db9930da445b22ddb9e2ceab7f.txt');
define('BASEPATH', __DIR__);

$history = file_get_contents(BASEPATH.'/'.HISTORY_FILE);

$db = new DB();
$scraper = new Scraper($db);
?>