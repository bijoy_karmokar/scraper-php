<?php
require 'init.php';
$sql = "SELECT * FROM schedule WHERE method = 'weekly'";
$result = $db->query($sql, 1);

if ($result['result'] != null) {
	$data = $result['result'];
	$daily = json_decode($data->post_obj);
	$today = date('Y-m-d');
	$yesterday = date('Y-m-d', strtotime('-7 day', strtotime($today)));

	$daily->dateFrom = $yesterday;
	$daily->dateTo = $today;
	try {
		$post = $scraper->get_curl(json_encode($daily), true, true, false);
		if ($post) {
			$data->last_exec = date('Y-m-d H:i:s');
			$result = $db->update('schedule', $data, 'method', 'weekly');
			if (!$result['error']) {
				echo 'weekly corn updated';
			} else {
				echo $result['result'];
			}
		}
	} catch (Exception $e) {
		var_dump('catch error: ', $e);
	}
} else {
	echo "Weekly cron settings is not updated in dateabase";
}
?>